package edu.tpc;

import cn.licoy.encryptbody.annotation.EnableEncryptBody;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName cn.tohxd.Application
 * @Author 赫尔墨斯·狄
 * @Date 2020/7/6 0006 11:23
 */
@SpringBootApplication
@EnableEncryptBody
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
