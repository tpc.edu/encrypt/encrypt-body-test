package edu.tpc.controller;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.AESEncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import edu.tpc.dto.UserDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LiuDong
 */
@RestController
@DecryptBody(DecryptBodyMethod.AES)
public class ApiController {

    @PostMapping("/test1")
    @AESEncryptBody
    public UserDto test1(@RequestBody UserDto userDto) {
        System.out.println(userDto.toString());
        return userDto;
    }
}
