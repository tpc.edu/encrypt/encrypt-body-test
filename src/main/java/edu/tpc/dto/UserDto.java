package edu.tpc.dto;

/**
 * @ClassName cn.tohxd.dto.UserDto
 * @Author 赫尔墨斯·狄
 * @Date 2020/7/6 0006 12:53
 */
public class UserDto {

    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "cn.tohxd.dto.UserDto{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
